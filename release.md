## Manual release process

1. Put the binaries into dist/
2. Bump version in package.json
3. Execute: npm publish
